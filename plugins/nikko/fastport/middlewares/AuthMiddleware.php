<?php namespace Nikko\Fastport\Middlewares;
session_start();

use Closure;
use Illuminate\Foundation\Application;
// use Illuminate\Http\Response;
// use October\Rain\Exception\AjaxException;
// use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class AuthMiddleware
{
/**
     * The Laravel Application
     *
     * @var Application
     */
    protected $app;

    /**
     * Create a new middleware instance.
     *
     * @param  Application $app
     * @return void
     */
    public function __construct()
    {
        // $this->app = $app;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        if($request->path() === '/') {
            session_destroy();
            return $next($request);
        } else {
            if($request->path() !== 'admin' && (!isset($_SESSION) || empty($_SESSION))) {
                return \Redirect::to('/');
            }
            return $next($request);
        }
    }
}