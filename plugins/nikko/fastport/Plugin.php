<?php namespace Nikko\Fastport;

use Backend;
use System\Classes\PluginBase;

/**
 * Fastport Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Fastport',
            'description' => 'No description provided yet...',
            'author'      => 'Nikko',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $this->app['Illuminate\Contracts\Http\Kernel']
          ->pushMiddleware('Nikko\Fastport\Middlewares\AuthMiddleware');
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Nikko\Fastport\Components\PageForm' => 'pageForm',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'nikko.fastport.some_permission' => [
                'tab' => 'Fastport',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'fastport' => [
                'label'       => 'Fastport',
                'url'         => Backend::url('nikko/fastport/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['nikko.fastport.*'],
                'order'       => 500,
            ],
        ];
    }
}
