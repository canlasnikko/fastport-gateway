<?php namespace Nikko\Fastport\Helpers;

use GuzzleHttp\Client;

class Common
{
  public function call_api($url, $json=[], $method='POST', $oAuth='')
  { 
    $client = new Client();
    $vars = [
      'http_errors' => false,
      'verify' => (boolean) env('RESOURCE_SSL'),
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => env('RESOURCE_ACCEPT'),
        'Authorization' => $oAuth
      ],
      'json' => $json
    ];
    
    $client = $client->request($method, $url, $vars);
    
    // LoggingHelper::logs('Helper/resource_api_with_auth','REQ: '. json_encode($vars) .' | RESPOND: '. json_encode($client) .' | starttime: '.$starttime.' | endtime: '.$endtime, 'INFO' ); 
    // $response = [];
    return $client;
  }
}