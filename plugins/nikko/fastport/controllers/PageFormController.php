<?php namespace Nikko\Fastport\Controllers;

use Session;
use BackendMenu;
use Backend\Classes\Controller;
use Nikko\Fastport\Helpers\Common;

/**
 * Page Form Controller Back-end Controller
 */
class PageFormController extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Nikko.Fastport', 'fastport', 'pageformcontroller');
    }

    public function logout()
    {
        $common = new Common();
        $url = env('UMS_DOMAIN').'/user/logout';
        $oAuth = $_SESSION['token_type'] . ' ' . $_SESSION['access_token'];

        $response = $common->call_api($url, [], $method='POST', $oAuth);
        $response_code = $response->getStatusCode();
        $response_body = json_decode($response->getBody()->getContents());

        session_destroy();

        return \Redirect::to('/');
    }
}
