<?php namespace Nikko\Fastport\Components;
session_start();

use Cms\Classes\ComponentBase;
use Request;
use Illuminate\Support\MessageBag;
use Nikko\Fastport\Helpers\Common;

class PageForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'PageForm Component',
            'description' => 'Component for Page Form'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onLoginAttempt()
    {
        $rules = [
            'username'        =>  ['required'],
            'password'        =>  ['required']         
        ];

        $validator = app('validator')->make(post(), $rules);

        if($validator->fails()) {
          $this->page["response_text"] = 'Complete all required fields';
          return false;
        }

        $common = new Common();
        $url = env('UMS_DOMAIN').'/user/login';
        $request = [
            'username'      =>  post('username'),
            'password'      =>  post('password'),
            'client_id'     =>  env('CLIENT_ID'),
            'client_secret' =>  env('CLIENT_SECRET')
        ];

        $response = $common->call_api($url, $request, $method='POST', $oAuth='');
        $response_code = $response->getStatusCode();
        $response_body = json_decode($response->getBody()->getContents());

        if($response_code !== 200){
            $this->page['response_text'] = 'Authentication Failed';
            return false;
        }

        if(isset($response_body->code) && $response_body->code !== 200) {
            $this->page['response_text'] = $response_body->error_message;
            return false;
        }

        $_SESSION['token_type'] = $response_body->token_type;
        $_SESSION['access_token'] = $response_body->access_token;

        return \Redirect::to('/profile');
    }
}
